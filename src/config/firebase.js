import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyB-K250IhBJYYVZp4zCBgKaGQRblWunQAM",
  authDomain: "chousei-firebase-cbac1.firebaseapp.com",
  databaseURL: "https://chousei-firebase-cbac1.firebaseio.com",
  projectId: "chousei-firebase-cbac1",
  storageBucket: "",
  messagingSenderId: "579930022664",
  appId: "1:579930022664:web:3795d2cf78650828"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
